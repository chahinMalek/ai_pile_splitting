from random import randint


class Game:

    _AI = 'AI'
    _PLAYER = 'Player'

    def __init__(self, coins: int):
        self.piles = [coins]
        self.players = [Game._AI, Game._PLAYER]
        self.turn = 0

    def _divide_pile(self, index: int, count: int):

        if index < 0 or self.piles[index] <= 2:
            raise ValueError('Could not divide pile.')

        pile = self.piles[index]
        pc = pile-count

        if count < pile and count != pc:
            self.piles.pop(index)
            self.piles.append(count)
            self.piles.append(pile - count)

        else:
            raise ValueError('Invalid count number.')

    def _divide_and_return_pile(self, piles, index: int, count: int, valid_piles: int):

        if piles[index] <= 2:
            raise ValueError('Could not divide pile.')

        pile = piles[index]
        pc = pile-count

        if count != pc:

            l = [x for x in piles]
            l.pop(index)
            valid_piles -= 1

            if count > 2:
                l.append(count)
                valid_piles += 1

            if pc > 2:
                l.append(pc)
                valid_piles += 1

            return l, valid_piles
        else:
            raise ValueError('Invalid count number.')

    def _play_ai_rec(self, piles, maximizer: bool, valid_piles: int):

        if valid_piles == 0:
            return 1 if not maximizer else -1

        score = -1 if maximizer else 1

        for i in range(len(piles)):
            pile_half = piles[i] // 2

            if piles[i] % 2:
                pile_half += 1

            for j in range(1, pile_half):

                divided_piles, valid_piles = self._divide_and_return_pile(piles, i, j, valid_piles)
                evaluation = self._play_ai_rec(divided_piles, not maximizer, valid_piles)

                # prune if a win is found, no need to check further
                if maximizer and evaluation == 1:
                    return evaluation

                # prune if a win is found, no need to check further
                if not maximizer and evaluation == -1:
                    return evaluation

        return score

    def _play_ai(self):

        piles = [x for x in self.piles]
        valid_piles = sum(1 for x in piles if x > 2)
        index = -1
        count = -1

        for i in range(len(piles)):
            pile_half = piles[i] // 2

            if piles[i] % 2:
                pile_half += 1

            for j in range(1, pile_half):
                try:
                    divided_piles, valid_piles = self._divide_and_return_pile(piles, i, j, valid_piles)
                    evaluation = self._play_ai_rec(divided_piles, False, valid_piles)
                except ValueError:
                    continue

                # prune if a win is found, no need to check further
                if evaluation > 0:
                    return i, j

        return index, count

    def play_move(self):

        # Player 1 move
        if self.players[self.turn] == Game._AI:

            index, count = self._play_ai()

            if index == -1:

                print('{}: I will lose so I will play something random :('.format(Game._AI))

                for i in range(len(self.piles)):
                    if self.piles[i] >= 3:

                        pile_half = self.piles[i] // 2

                        if self.piles[i] % 2 == 0:
                            pile_half -= 1

                        count = randint(1, pile_half)
                        index = i
                        break

            self._divide_pile(index, count)

        # Player 2 move
        else:
            index = int(input('Choose pile: '))
            count = int(input('Choose count: '))
            self._divide_pile(index, count)

        if self.check_win():
            print('{} won!'.format(self.players[self.turn]))
            return

        # change player turn
        self.turn ^= 1

    def check_win(self):
        return True if len([x for x in self.piles if x > 2]) == 0 else False

    def print_board(self):
        print(self.piles.__str__())
