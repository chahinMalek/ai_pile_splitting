from game import Game

game = Game(5)

while True:

    print(100*'-')
    print('Board status: ', end='')
    game.print_board()

    if game.check_win():
        break

    print('{}\'s move'.format(game.players[game.turn]))
    ok = False

    while not ok:
        try:
            game.play_move()
            ok = True
        except (ValueError, IndexError):
            ok = False
